FROM tensorflow/tensorflow:latest-gpu-py3

RUN pip install --upgrade pip
RUN pip install numpy sklearn pandas spektral

WORKDIR /eeg
ENV PYTHONPATH=".:${PYTHONPATH}"

COPY ./src ./src
