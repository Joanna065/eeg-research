import json
import logging
import os
import pickle

import numpy as np

log = logging.getLogger(__name__)


def open_pickle(filepath):
    try:
        with open(filepath, "rb") as file:
            data = pickle.load(file)
            log.info(f'Loading data from file {filepath}')
            return data
    except IOError:
        log.error(f'Failed to open {filepath} file', exc_info=True)
        exit(1)


def save_pickle(filepath, data):
    try:
        with open(filepath, "wb") as file:
            pickle.dump(data, file)
            log.info(f'Saving data to file {filepath}')
    except IOError:
        log.error(f'Failed to create {filepath} file', exc_info=True)
        exit(1)


def open_json(filepath):
    try:
        with open(filepath, "rb") as file:
            data = json.load(file)
            log.info(f'Loading data from file {filepath}')
            return data
    except IOError:
        log.error(f'Failed to open {filepath} file', exc_info=True)
        exit(1)


def save_dict_to_npz(filepath, data_dict):
    try:
        with open(filepath, "wb") as file:
            log.info(f'Writing data to file {filepath}')
            np.savez(file, **data_dict)
    except IOError:
        logging.error(f'Failed to save into {filepath} file', exc_info=True)
        exit(1)


def load_from_npz(filepath):
    try:
        return np.load(filepath)
    except IOError:
        log.error(f'Failed to open {filepath} file', exc_info=True)
        exit(1)


def ensure_dir_exists(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)


def save_model_summary(dirpath, model_name, model):
    with open(dirpath + '/{}.txt'.format(model_name), 'w') as f:
        model.summary(print_fn=lambda x: f.write(x + '\n'))
