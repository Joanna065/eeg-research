import pandas as pd
import tensorflow as tf
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator
from tensorboard.plugins.hparams import plugin_data_pb2
from tensorflow.python.summary.summary_iterator import summary_iterator
from tqdm import tqdm


class TensorboardHParamsParser:
    def __init__(self, log_dir):
        self.log_dir = log_dir
        self.hparams_df = pd.DataFrame()

    def parse_to_df(self):
        run_dirs = list(self.log_dir.iterdir())
        for run_dir in tqdm(run_dirs, desc='Reading event run dirs'):
            if run_dir.is_dir() and run_dir.name != 'curves':
                for event_file in run_dir.iterdir():
                    tags = self._get_tags(event_file)
                    si = summary_iterator(str(event_file))
                    event_row_dict = {}

                    for event in si:
                        for value in event.summary.value:
                            proto_bytes = value.metadata.plugin_data.content
                            plugin_data = plugin_data_pb2.HParamsPluginData.FromString(proto_bytes)

                            for tag in tags:
                                if value.tag == tag:
                                    tag_value = tf.make_ndarray(value.tensor)
                                    event_row_dict[tag] = tag_value

                            if plugin_data.HasField("session_start_info"):
                                hparams = dict(plugin_data.session_start_info.hparams)
                                for key, val in hparams.items():
                                    event_row_dict[key] = val.number_value
                    self.hparams_df = self.hparams_df.append(event_row_dict, ignore_index=True)
        self.hparams_df.dropna(inplace=True)

    def save_df(self, filepath):
        self.hparams_df.drop(columns=['_hparams_/session_start_info'], inplace=True)
        self.hparams_df.reset_index(inplace=True, drop=True)
        self.hparams_df.to_csv(filepath, index=False)

    @staticmethod
    def _get_tags(event_file):
        event_acc = EventAccumulator(str(event_file))
        event_acc.Reload()
        return event_acc.Tags()['tensors']
