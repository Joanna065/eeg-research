from pathlib import Path

PROJECT_DIR = Path(__file__).parent.parent.absolute()

DATA_DIR = PROJECT_DIR / 'data'
RESULT_DIR = PROJECT_DIR / 'results'

try:
    from src.user_settings import *
except ImportError:
    pass
