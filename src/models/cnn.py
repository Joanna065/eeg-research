import tensorflow as tf


def build_cnn_model(shape, class_num, hidden_units, dropout, filters, kernel):
    N = shape[2]
    F = shape[3]
    frames = shape[1]

    def get_frame(x, frame, N, F):
        x = tf.slice(x, [0, frame, 0, 0], [-1, 1, N, F])
        x = tf.squeeze(x, axis=[1])
        return x

    input_0 = tf.keras.Input((frames, N, F))

    layers = []
    for frame in range(frames):
        frame_matrix = tf.keras.layers.Lambda(
            get_frame,
            arguments={'frame': frame, 'N': N, 'F': F}
        )(input_0)

        x = tf.keras.layers.Conv1D(filters, kernel, data_format='channels_first')(frame_matrix)
        x = tf.keras.layers.Flatten()(x)
        layers.append(x)

    combine = tf.keras.layers.Concatenate()(layers)
    reshape = tf.keras.layers.Reshape((frames, -1))(combine)
    lstm = tf.keras.layers.LSTM(hidden_units)(reshape)
    dropout = tf.keras.layers.Dropout(dropout)(lstm)
    out = tf.keras.layers.Dense(class_num, activation='softmax')(dropout)

    model = tf.keras.Model(inputs=[input_0], outputs=out)
    return model
