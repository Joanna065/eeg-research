import spektral as sp
import tensorflow as tf


def build_gnn_model(shape, class_num, output_channels, hidden_units, dropout):
    N = shape[2]
    F = shape[3] - N
    frames = shape[1]

    def get_feature_matrix(x, frame, N, F):
        x = tf.slice(x, [0, frame, 0, N], [-1, 1, N, F])
        x = tf.squeeze(x, axis=[1])
        return x

    def get_correlation_matrix(x, frame, N):
        x = tf.slice(x, [0, frame, 0, 0], [-1, 1, N, N])
        x = tf.squeeze(x, axis=[1])
        return x

    input_0 = tf.keras.Input((frames, N, F + N))

    layers = []
    for frame in range(frames):
        feature_matrix = tf.keras.layers.Lambda(
            get_feature_matrix,
            arguments={'frame': frame, 'N': N, 'F': F}
        )(input_0)

        correlation_matrix = tf.keras.layers.Lambda(
            get_correlation_matrix,
            arguments={'frame': frame, 'N': N}
        )(input_0)

        x = sp.layers.GraphConv(output_channels)([feature_matrix, correlation_matrix])
        x = tf.keras.layers.Flatten()(x)
        layers.append(x)

    combine = tf.keras.layers.Concatenate()(layers)
    reshape = tf.keras.layers.Reshape((frames, N * output_channels))(combine)
    lstm = tf.keras.layers.LSTM(hidden_units)(reshape)
    dropout = tf.keras.layers.Dropout(dropout)(lstm)
    out = tf.keras.layers.Dense(class_num, activation='softmax')(dropout)

    model = tf.keras.Model(inputs=[input_0], outputs=out)
    return model
