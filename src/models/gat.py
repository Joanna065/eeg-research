from typing import Tuple

import spektral as sp
import tensorflow as tf


def build_gat_model(shape: Tuple[int, int, int, int], class_num: int, output_channels: int = 64,
                    hidden_units: int = 64, dropout: float = 0.2) -> tf.keras.Model:
    assert len(shape) == 4

    batch_size, frames, channels, features = shape
    features = features - channels

    input_0 = tf.keras.Input((frames, channels, features + channels))

    layers = []
    for frame in range(frames):
        feature_matrix = tf.keras.layers.Lambda(
            get_feature_matrix,
            arguments={'frame_num': frame, 'channels': channels, 'features': features}
        )(input_0)

        correlation_matrix = tf.keras.layers.Lambda(
            get_correlation_matrix,
            arguments={'frame_num': frame, 'channels': channels}
        )(input_0)

        x = sp.layers.GraphAttention(output_channels)([feature_matrix, correlation_matrix])
        x = tf.keras.layers.Flatten()(x)
        layers.append(x)

    combine = tf.keras.layers.Concatenate()(layers)
    reshape = tf.keras.layers.Reshape((frames, channels * output_channels))(combine)

    lstm = tf.keras.layers.LSTM(hidden_units)(reshape)
    dropout = tf.keras.layers.Dropout(dropout)(lstm)

    out = tf.keras.layers.Dense(class_num, activation='softmax')(dropout)

    return tf.keras.Model(inputs=[input_0], outputs=out)


def get_feature_matrix(x: tf.Tensor, frame_num: int, channels: int, features: int) -> tf.Tensor:
    x = tf.slice(x, [0, frame_num, 0, channels], [-1, 1, channels, features])
    x = tf.squeeze(x, axis=[1])
    return x


def get_correlation_matrix(x: tf.Tensor, frame_num: int, channels: int) -> tf.Tensor:
    x = tf.slice(x, [0, frame_num, 0, 0], [-1, 1, channels, channels])
    x = tf.squeeze(x, axis=[1])
    return x
