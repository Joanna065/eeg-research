import tensorflow as tf
from tensorflow.keras import regularizers


class LSTM(tf.keras.Model):
    def __init__(self, hidden_units=256, d0=0., d1=0., d2=0., l2=0.001, num_classes=2,
                 name='lstm', **kwargs):
        super(LSTM, self).__init__(name=name, **kwargs)
        self.input_dropout = tf.keras.layers.Dropout(rate=d0)
        self.lstm_1 = tf.keras.layers.LSTM(hidden_units,
                                           return_sequences=True,
                                           name='lstm_1',
                                           activation='tanh',
                                           dropout=d1,
                                           kernel_regularizer=regularizers.l2(l2))
        self.lstm_2 = tf.keras.layers.LSTM(hidden_units,
                                           return_sequences=False,
                                           name='lstm_2',
                                           activation='tanh',
                                           dropout=d2,
                                           kernel_regularizer=regularizers.l2(l2))
        self.classifier = tf.keras.layers.Dense(units=num_classes,
                                                name='dense_classifier',
                                                activation='softmax')

    def call(self, inputs, **kwargs):
        x = self.input_dropout(inputs)
        x = self.lstm_1(x)
        x = self.lstm_2(x)
        return self.classifier(x)
