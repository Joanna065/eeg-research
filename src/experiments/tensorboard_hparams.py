from typing import Dict, List, Optional, Tuple

import numpy as np
import pandas as pd
import tensorflow as tf
from tensorboard.plugins.hparams import api as hp


def run_trial(run_dir: str, model: tf.keras.Model, step: int, hparams: Dict, data, epochs: int = 50,
              batch_size: int = 32, callbacks: List[tf.keras.callbacks.Callback] = None,
              patience: Optional[int] = None, cv_run: Optional[int] = None):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)

        if len(data) == 6:
            x_train, y_train, x_val, y_val, x_test, y_test = data
            model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, shuffle=True,
                      callbacks=callbacks, validation_data=(x_val, y_val), verbose=1)

        elif len(data) == 4:
            x_train, y_train, x_test, y_test = data
            model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, shuffle=True,
                      callbacks=callbacks)
        else:
            raise ValueError("Wrong tuple with data!")

        history = model.evaluate(x_test, y_test, verbose=1)

        if patience is not None:
            tf.summary.scalar('patience', patience, step=step)

        if cv_run is not None:
            tf.summary.scalar('cv_run', cv_run, step=step)

        tf.summary.scalar('accuracy', history[1], step=step)
        class_nr = 0
        for i in range(2, len(history), 3):
            tf.summary.scalar(f'recall_{class_nr}', history[i], step=step)
            tf.summary.scalar(f'precision_{class_nr}', history[i + 1], step=step)
            tf.summary.scalar(f'f1score_{class_nr}', history[i + 2], step=step)
            class_nr += 1


def set_global_hparams_config(run_dir: str, hparams: Dict, num_classes: int,
                              patience: bool = False) -> None:
    with tf.summary.create_file_writer(run_dir).as_default():
        cm_metrics = [(hp.Metric(f'recall_{nr}', display_name=f'recall_{nr}'),
                       hp.Metric(f'precision_{nr}', display_name=f'precision_{nr}'),
                       hp.Metric(f'f1score_{nr}', display_name=f'f1score_{nr}')) for nr in
                      range(num_classes)]
        cm_metrics = [item for sublist in cm_metrics for item in sublist]

        hp.hparams_config(
            hparams=list(hparams.values()),
            metrics=[hp.Metric('accuracy', display_name='accuracy'),
                     hp.Metric('patience', display_name='patience'),
                     *cm_metrics] if patience else
            [hp.Metric('accuracy', display_name='accuracy'), *cm_metrics]
        )


def get_hparams_from_dataframe(hparams_df: pd.DataFrame) -> Tuple[List, Dict]:
    hparams_list = hparams_df.to_dict('records')

    hparams_unique = {
        col: hp.HParam(col, hp.Discrete([val.item() for val in list(np.unique(hparams_df[col]))]))
        for col in hparams_df.columns
    }

    return hparams_list, hparams_unique
