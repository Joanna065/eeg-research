import itertools
import random
from pathlib import Path

import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorboard.plugins.hparams import api as hp
from tensorflow.python.keras.metrics import Precision, Recall
from tensorflow.python.keras.utils.np_utils import to_categorical

from src.experiments.tensorboard_hparams import run_trial, set_global_hparams_config
from src.learning.metrics_callback import F1Score
from src.models.cbam import build_cbam_model
from src.settings import DATA_DIR, RESULT_DIR
from src.utils.io import open_pickle

DATASET = 'tuheeg_abnormal'
DATASET_FILE = f'{DATASET}_two_classes'
MODEL_NAME = 'CBAM'
DATASET_PATH = DATA_DIR / 'processed' / (DATASET_FILE + '.pkl')

SAVE_HPARAMS = f'{DATASET}_2class_50epoch'
TENSORBOARD_DIR = RESULT_DIR / 'logs' / 'tensorboard' / 'tuning' / MODEL_NAME / SAVE_HPARAMS
TENSORBOARD_DIR.mkdir(parents=True, exist_ok=True)

# load data
dataset = open_pickle(DATASET_PATH)
samples, labels = dataset['data'], dataset['labels']
print(f'{DATASET}: {samples.shape}')
x_train, x_test, y_train, y_test = train_test_split(samples, labels, test_size=0.1, stratify=labels)

# convert labels to onehot
classes_num = len(np.sort(np.unique(labels)))
y_train = to_categorical(y_train, num_classes=classes_num)
y_test = to_categorical(y_test, num_classes=classes_num)

# hyper params to be tuned
hparams = {
    'lr': [1e-4, 5e-4, 1e-3],
    'hidden_units': [8, 16, 32, 64, 128, 256],
    'filters': [4, 8, 16, 32],
    'kernel': [3, 5, 7, 11],
    'dropout': [0.20, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5],
    'reduction_ratio': [4, 8, 16],
    'spatial_kernel': [5, 7, 11]
}

# hparams = {
#     'lr': [1e-4, 5e-4, 1e-3],
#     'hidden_units': [8, 16, 32, 64, 128, 256],
#     'filters': [4, 8, 16, 32],
#     'kernel': [3, 5, 7, 11],
#     'dropout': [0.00, 0.05, 0.10, 0.15, 0.20, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5]
# }

hparams = {
    key: hp.HParam(key, hp.Discrete(value))
    for key, value in hparams.items()
}

set_global_hparams_config(str(TENSORBOARD_DIR), hparams, classes_num)

hparams_combinations = itertools.product(*[h.domain.values for h in hparams.values()])
hparams_combinations = [val for val in hparams_combinations if val[2] >= val[5]]
# hparams_combinations = [val for val in hparams_combinations]
random.shuffle(hparams_combinations)

session_num = 0
for params_config in hparams_combinations:
    hparams = {
        'lr': params_config[0],
        'hidden_units': params_config[1],
        'filters': params_config[2],
        'kernel': params_config[3],
        'dropout': params_config[4],
        'reduction_ratio': params_config[5],
        'spatial_kernel': params_config[6]
    }
    # get model
    model = build_cbam_model(shape=samples.shape, classes=classes_num, dropout=hparams['dropout'],
                             filters=hparams['filters'], kernel=hparams['kernel'],
                             hidden_units=hparams['hidden_units'],
                             att_ratio=hparams['reduction_ratio'],
                             att_kernel=hparams['spatial_kernel'])

    cm_metrics = [(Recall(class_id=nr, name=f'recall_{nr}'),
                   Precision(class_id=nr, name=f'precision_{nr}'),
                   F1Score(class_id=nr, name=f'f1score_{nr}')) for nr in range(classes_num)]

    cm_metrics = [item for sublist in cm_metrics for item in sublist]

    model.compile(loss='categorical_crossentropy',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=hparams['lr']),
                  metrics=['accuracy', *cm_metrics])

    # run and log tuning trial
    run_name = f'run-{session_num}'
    print(f'-- Starting trial: {run_name} --')
    print({key: val for key, val in hparams.items()})

    data = x_train, y_train, x_test, y_test
    run_trial(str(Path(TENSORBOARD_DIR, f'{run_name}')), model, session_num, hparams, data=data,
              epochs=50, batch_size=32)
    session_num += 1
