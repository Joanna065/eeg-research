from pathlib import Path

import numpy as np
import tensorflow as tf
from sklearn.model_selection import StratifiedKFold
from tensorboard.plugins.hparams import api as hp
from tensorflow.python.keras.metrics import Precision, Recall
from tensorflow.python.keras.utils.np_utils import to_categorical

from src.experiments.tensorboard_hparams import run_trial, set_global_hparams_config
from src.learning.metrics_callback import F1Score
from src.models.cbam import build_cbam_model
from src.settings import DATA_DIR, PROJECT_DIR, RESULT_DIR
from src.utils.io import open_json, open_pickle

DATASET = 'tuheeg_abnormal'
DATASET_FILE = f'{DATASET}_two_classes'
MODEL_NAME = 'CBAM'
DATASET_PATH = DATA_DIR / 'processed' / (DATASET_FILE + '.pkl')

SAVE_HPARAMS = f'{DATASET}_best_hparams_from_both'
TENSORBOARD_DIR = RESULT_DIR / 'logs' / 'tensorboard' / 'cross_val' / MODEL_NAME / SAVE_HPARAMS
TENSORBOARD_DIR.mkdir(parents=True, exist_ok=True)

# load data
dataset = open_pickle(DATASET_PATH)
samples, labels = dataset['data'], dataset['labels']
print(f'{DATASET}: {samples.shape}')

classes_num = len(np.sort(np.unique(labels)))

# get chosen hparams
hparams_list = open_json(PROJECT_DIR / 'src/experiments/hparams/conv/cv_cbam_best_from_both.json')
hparams_list = hparams_list['hparams']
hparams_keys = hparams_list[0].keys()
hparams_unique = {
    key: hp.HParam(key, hp.Discrete(set([d[key] for d in hparams_list]))) for key in hparams_keys
}
set_global_hparams_config(str(TENSORBOARD_DIR), hparams_unique, classes_num)

# specify cross validation folds number
N_SPLITS = 10
EPOCHS = 50
BATCH_SIZE = 32

idx = 0
for hparams in hparams_list:
    skfold = StratifiedKFold(n_splits=N_SPLITS, shuffle=True)

    # for cv_run in range(0, 10):
    for train_idx, test_idx in skfold.split(samples, labels):
        # prepare data
        x_train, y_train = samples[train_idx], labels[train_idx]
        x_test, y_test = samples[test_idx], labels[test_idx]

        y_train = to_categorical(y_train, num_classes=classes_num)
        y_test = to_categorical(y_test, num_classes=classes_num)

        # get model
        model = build_cbam_model(shape=samples.shape, class_num=classes_num,
                                 dropout=hparams['dropout'], filters=hparams['filters'],
                                 kernel=hparams['kernel'], hidden_units=hparams['hidden_units'],
                                 reduce_ratio=hparams['reduction_ratio'],
                                 spatial_kernel=hparams['spatial_kernel'])

        cm_metrics = [(Recall(class_id=nr, name=f'recall_{nr}'),
                       Precision(class_id=nr, name=f'precision_{nr}'),
                       F1Score(class_id=nr, name=f'f1score_{nr}')) for nr in range(classes_num)]

        cm_metrics = [item for sublist in cm_metrics for item in sublist]

        model.compile(loss='categorical_crossentropy',
                      optimizer=tf.keras.optimizers.Adam(learning_rate=hparams['lr']),
                      metrics=['accuracy', *cm_metrics])

        # run and log tuning trial
        run_name = f'run-{idx}'
        print(f'-- Starting trial: {run_name} --')
        print({key: val for key, val in hparams.items()})
        data = x_train, y_train, x_test, y_test

        run_trial(str(Path(TENSORBOARD_DIR, f'{run_name}')), model, idx, hparams, data=data,
                  epochs=EPOCHS, batch_size=BATCH_SIZE)
        idx += 1
