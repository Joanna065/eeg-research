import itertools
import os
import random
from pathlib import Path

import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorboard.plugins.hparams import api as hp
from tensorflow.python.keras.metrics import Precision, Recall
from tensorflow.python.keras.utils.np_utils import to_categorical

from src.experiments.tensorboard_hparams import run_trial, set_global_hparams_config
from src.learning.metrics_callback import F1Score
from src.models.gat import build_gat_model
from src.settings import DATA_DIR, RESULT_DIR
from src.utils.io import open_pickle

DATASET_NAME = 'tuheeg_abnormal_two_classes'
MODEL_NAME = 'GAT'
DATASET_PATH = os.path.join(DATA_DIR, 'processed', DATASET_NAME + '.pkl')

SAVE_HPARAMS = 'abnormal_two_classes_50epoch'
TENSORBOARD_DIR = Path(RESULT_DIR, 'logs/tensorboard/tuning', MODEL_NAME, SAVE_HPARAMS)
TENSORBOARD_DIR.mkdir(parents=True, exist_ok=True)

# load data
dataset = open_pickle(DATASET_PATH)
samples, labels = dataset['data'], dataset['labels']
print(f'{DATASET_NAME}: {samples.shape}')
x_train, x_test, y_train, y_test = train_test_split(samples, labels, test_size=0.1, stratify=labels)

# convert labels to onehot
classes_num = len(np.sort(np.unique(labels)))
y_train = to_categorical(y_train, num_classes=classes_num)
y_test = to_categorical(y_test, num_classes=classes_num)

# hyper params to be tuned
hparams = {
    'lr': [1e-4, 5e-4, 1e-3],
    'hidden_units': [8, 16, 32, 64],
    'output_shape': [8, 16, 32, 64],
    'dropout': [0.00, 0.05, 0.10, 0.15, 0.20],
}

hparams = {
    key: hp.HParam(key, hp.Discrete(value))
    for key, value in hparams.items()
}

set_global_hparams_config(str(TENSORBOARD_DIR), hparams, classes_num)

hparams_combinations = list(
    itertools.product(*[h.domain.values for h in hparams.values()]))
random.shuffle(hparams_combinations)

session_num = 0
for params_config in hparams_combinations:
    hparams = {
        'lr': params_config[0],
        'hidden_units': params_config[1],
        'output_shape': params_config[2],
        'dropout': params_config[3]
    }
    # get model
    model = build_gat_model(shape=samples.shape, classes=classes_num, dropout=hparams['dropout'],
                            output_shape=hparams['output_shape'],
                            hidden_units=hparams['hidden_units'])

    cm_metrics = [(Recall(class_id=nr, name=f'recall_{nr}'),
                   Precision(class_id=nr, name=f'precision_{nr}'),
                   F1Score(class_id=nr, name=f'f1score_{nr}')) for nr in range(classes_num)]

    cm_metrics = [item for sublist in cm_metrics for item in sublist]

    model.compile(loss='categorical_crossentropy',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=hparams['lr']),
                  metrics=['accuracy', *cm_metrics])

    # run and log tuning trial
    run_name = f'run-{session_num}'
    print(f'-- Starting trial: {run_name} --')
    print({key: val for key, val in hparams.items()})

    data = x_train, y_train, x_test, y_test
    run_trial(str(Path(TENSORBOARD_DIR, f'{run_name}')), model, session_num, hparams, data=data,
              epochs=50, batch_size=32)
    session_num += 1
