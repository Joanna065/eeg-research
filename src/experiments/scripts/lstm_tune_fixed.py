from pathlib import Path

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.metrics import Precision, Recall
from tensorflow.python.keras.utils.np_utils import to_categorical

from src.experiments.tensorboard_hparams import (get_hparams_from_dataframe, run_trial,
                                                 set_global_hparams_config)
from src.learning.metrics_callback import F1Score
from src.models.lstm_att import AttentionLSTM
from src.settings import DATA_DIR, RESULT_DIR
from src.utils.io import load_from_npz

DATASET = 'tuheeg_abnormal'
DATASET_FILE = f'{DATASET}_two_classes'
MODEL_NAME = 'LSTMAtt'
DATASET_PATH = DATA_DIR / 'datasets' / (DATASET_FILE + '.npz')

SAVE_HPARAMS = f'fixed_{DATASET}_2layer_2class_50epoch'
TENSORBOARD_DIR = RESULT_DIR / 'logs' / 'tensorboard' / 'tuning' / MODEL_NAME / SAVE_HPARAMS
TENSORBOARD_DIR.mkdir(parents=True, exist_ok=True)

# load data
data = load_from_npz(DATASET_PATH)
samples, labels = data['samples'], data['labels']
print(f'{DATASET}: {samples.shape}')

amount, timeframes, features = samples.shape
x_train, x_test, y_train, y_test = train_test_split(samples, labels, test_size=0.1, stratify=labels)

# convert labels to onehot
num_classes = len(np.sort(np.unique(labels)))
y_train = to_categorical(y_train, num_classes=num_classes)
y_test = to_categorical(y_test, num_classes=num_classes)

# get tensorboard HParams
hparams_merged = pd.read_csv(Path(RESULT_DIR, 'tuning', 'lstm_2layer_fixed_hparams.csv'))
selected_hparams_list, hparams_unique = get_hparams_from_dataframe(hparams_merged)
set_global_hparams_config(str(TENSORBOARD_DIR), hparams_unique, num_classes)

# tune model with fixed hyper parameters
session_num = 0
for hparams in selected_hparams_list:
    model = AttentionLSTM(hidden_units=hparams['hidden_units'], d0=hparams['dropout_0'],
                          d1=hparams['dropout_1'], d2=hparams['dropout_2'], l2=hparams['l2'],
                          num_classes=num_classes, timesteps=timeframes)

    cm_metrics = [(Recall(class_id=nr, name=f'recall_{nr}'),
                   Precision(class_id=nr, name=f'precision_{nr}'),
                   F1Score(class_id=nr, name=f'f1score_{nr}')) for nr in range(num_classes)]

    cm_metrics = [item for sublist in cm_metrics for item in sublist]

    model.compile(loss='categorical_crossentropy',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=hparams['lr']),
                  metrics=['accuracy', *cm_metrics])

    # run and log tuning trial
    run_name = f'run-{session_num}'
    print(f'-- Starting trial: {run_name} --')
    print({key: val for key, val in hparams.items()})

    data = x_train, y_train, x_test, y_test

    run_trial(str(Path(TENSORBOARD_DIR, f'{run_name}')), model, session_num, hparams, data=data,
              epochs=50, batch_size=32)
    session_num += 1
