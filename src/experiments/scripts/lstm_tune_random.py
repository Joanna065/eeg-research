import itertools
import random
from pathlib import Path

import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorboard.plugins.hparams import api as hp
from tensorflow.python.keras.metrics import Precision, Recall
from tensorflow.python.keras.utils.np_utils import to_categorical

from src.experiments.tensorboard_hparams import run_trial, set_global_hparams_config
from src.learning.metrics_callback import F1Score
from src.models.lstm_att import AttentionLSTM
from src.settings import DATA_DIR, RESULT_DIR
from src.utils.io import load_from_npz

DATASET = 'tuheeg_abnormal'
DATASET_FILE = f'{DATASET}_two_classes'
MODEL_NAME = 'LSTMAtt'
DATASET_PATH = DATA_DIR / 'datasets' / (DATASET_FILE + '.npz')

SAVE_HPARAMS = f'{DATASET}_2layer_2class_50epoch'
TENSORBOARD_DIR = RESULT_DIR / 'logs' / 'tensorboard' / 'tuning' / MODEL_NAME / SAVE_HPARAMS
TENSORBOARD_DIR.mkdir(parents=True, exist_ok=True)

# load data
data = load_from_npz(DATASET_PATH)
samples, labels = data['samples'], data['labels']
print(f'{DATASET}: {samples.shape}')

amount, timeframes, features = samples.shape
x_train, x_test, y_train, y_test = train_test_split(samples, labels, test_size=0.1, stratify=labels)

# convert labels to onehot
classes_num = len(np.sort(np.unique(labels)))
y_train = to_categorical(y_train, num_classes=classes_num)
y_test = to_categorical(y_test, num_classes=classes_num)

# get all configurations of hyper params
hparams = {
    'hidden_units': [32, 64, 128, 256],
    'dropout_0': [0., 0.1, 0.2],
    'dropout_1': [0., 0.1, 0.15, 0.2, 0.3, 0.4],
    'dropout_2': [0., 0.1, 0.15, 0.2, 0.3],
    'l2': [0.001, 0.005, 0.01, 0.02, 0.03, 0.04],
    'lr': [1e-4, 5e-4, 1e-3]
}
hparams = {
    key: hp.HParam(key, hp.Discrete(value))
    for key, value in hparams.items()
}
set_global_hparams_config(str(TENSORBOARD_DIR), hparams, classes_num)

hparams_combinations = list(
    itertools.product(*[h.domain.values for h in hparams.values()]))
random.shuffle(hparams_combinations)

session_num = 0
for params_config in hparams_combinations:
    hparams = {
        'hidden_units': params_config[0],
        'dropout_0': params_config[1],
        'dropout_1': params_config[2],
        'dropout_2': params_config[3],
        'l2': params_config[4],
        'lr': params_config[5]
    }
    # get model
    model = AttentionLSTM(hidden_units=hparams['hidden_units'], d0=hparams['dropout_0'],
                          d1=hparams['dropout_1'], d2=hparams['dropout_2'], l2=hparams['l2'],
                          num_classes=classes_num, timesteps=timeframes)

    cm_metrics = [(Recall(class_id=nr, name=f'recall_{nr}'),
                   Precision(class_id=nr, name=f'precision_{nr}'),
                   F1Score(class_id=nr, name=f'f1score_{nr}')) for nr in range(classes_num)]

    cm_metrics = [item for sublist in cm_metrics for item in sublist]

    model.compile(loss='categorical_crossentropy',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=hparams['lr']),
                  metrics=['accuracy', *cm_metrics])

    # run and log tuning trial
    run_name = f'run-{session_num}'
    print(f'-- Starting trial: {run_name} --')
    print({key: val for key, val in hparams.items()})

    data = x_train, y_train, x_test, y_test

    run_trial(str(Path(TENSORBOARD_DIR, f'{run_name}')), model, session_num, hparams, data=data,
              epochs=50, batch_size=32)
    session_num += 1
