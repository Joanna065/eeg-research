import logging
import os
from datetime import datetime
from pathlib import Path

import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.metrics import Precision, Recall
from tensorflow.python.keras.utils.np_utils import to_categorical

from src.learning.metrics_callback import F1Score
from src.models.lstm_att import AttentionLSTM
from src.settings import DATA_DIR, RESULT_DIR
from src.utils.io import load_from_npz

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

DATASET_NAME = 'seizure_2class_new'
MODEL_NAME = 'LSTMAtt'
DATASET_PATH = os.path.join(DATA_DIR, 'datasets', DATASET_NAME + '.npz')

date = datetime.today()
date_now = date.strftime('%Y-%m-%d_%H-%M')

# load data
data = load_from_npz(DATASET_PATH)
samples, labels = data['samples'], data['labels']
print(f'{DATASET_NAME}: {samples.shape}')

amount, timeframes, features = samples.shape

x_train, x_test, y_train, y_test = train_test_split(samples, labels, test_size=0.1, stratify=labels)
x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=0.1, stratify=y_train)

# convert labels to onehot
num_classes = len(np.sort(np.unique(labels)))
y_train = to_categorical(y_train, num_classes=num_classes)
y_val = to_categorical(y_val, num_classes=num_classes)
y_test = to_categorical(y_test, num_classes=num_classes)

model_params = {
    'hidden_units': 64,
    'd0': 0.1,
    'd1': 0.15,
    'd2': 0.2,
    'l2': 0.001,
    'timesteps': timeframes,
    'num_classes': num_classes
}

cm_metrics = [(Recall(class_id=nr, name=f'recall_{nr}'),
               Precision(class_id=nr, name=f'precision_{nr}'),
               F1Score(class_id=nr, name=f'f1score_{nr}')) for nr in range(num_classes)]

cm_metrics = [item for sublist in cm_metrics for item in sublist]

train_params = {
    'batch_size': 32,
    'epochs': 500,
    'loss': 'categorical_crossentropy',
    'optimizer': tf.keras.optimizers.Adam(learning_rate=0.0001),
    'metrics': ['accuracy', *cm_metrics],
    'onehot': True
}
# prepare save directory name
SAVE_MODEL = f'{date_now}_{MODEL_NAME}-units-{model_params["hidden_units"]}' \
             f'-d0-{model_params["d0"]}-d1-{model_params["d1"]}-d2-{model_params["d2"]}' \
             f'-l2-{model_params["l2"]}-{DATASET_NAME}'

CHECKPOINT_DIR = Path(RESULT_DIR, 'checkpoints', SAVE_MODEL)
TENSORBOARD_DIR = Path(RESULT_DIR, 'logs', 'tensorboard', 'train', SAVE_MODEL)
Path.mkdir(CHECKPOINT_DIR, parents=True, exist_ok=True)
Path.mkdir(TENSORBOARD_DIR, parents=True, exist_ok=True)

# callbacks
checkpoint_filepath = os.path.join(CHECKPOINT_DIR, 'weights')
model_checkpoint = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_filepath, save_weights_only=True,
    monitor='val_loss', mode='min', patience=10, save_best_only=True, verbose=1)
tensorboard = tf.keras.callbacks.TensorBoard(log_dir=TENSORBOARD_DIR, update_freq='epoch')

model = AttentionLSTM(**model_params)
model.run_eagerly = True
model.compile(loss=train_params['loss'], optimizer=train_params['optimizer'],
              metrics=train_params['metrics'])

print("Training model: ")
model.fit(x_train, y_train, epochs=train_params['epochs'], batch_size=train_params['batch_size'],
          validation_data=(x_val, y_val), shuffle=True, verbose=1,
          callbacks=[model_checkpoint, tensorboard])

print('Evaluating model: ')
model.load_weights(checkpoint_filepath)
test_loss, test_acc, test_recall, test_precision, test_fscore = model.evaluate(x_test, y_test,
                                                                               verbose=1)
print(f'TEST accuracy: {test_acc}, precision: {test_precision}, recall: {test_recall}, '
      f'fscore: {test_fscore}')
