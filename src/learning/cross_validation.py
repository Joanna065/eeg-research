import logging

import numpy as np
from sklearn.model_selection import StratifiedKFold
from tensorflow.python.keras.utils.np_utils import to_categorical

log = logging.getLogger(__name__)


def train_k_fold(data, labels, n_splits, model_cls, model_params, train_params, shuffle=True):
    skfold = StratifiedKFold(n_splits=n_splits, shuffle=shuffle)
    accuracy_list, recall_list, precision_list, fscore_list = [], [], [], []

    for fold_nr, (train_idx, test_idx) in enumerate(skfold.split(data, labels), 1):
        # prepare data
        x_train, y_train = data[train_idx], labels[train_idx]
        x_test, y_test = data[test_idx], labels[test_idx]

        classes_num = len(np.sort(np.unique(labels)))
        y_train = to_categorical(y_train, num_classes=classes_num)
        y_test = to_categorical(y_test, num_classes=classes_num)

        # build model
        model = model_cls(**model_params)
        model.compile(loss=train_params['loss'], optimizer=train_params['optimizer'],
                      metrics=train_params['metrics'])

        log.info(f'Training fold: {fold_nr}')
        model.fit(x_train, y_train, epochs=train_params['epochs'],
                  batch_size=train_params['batch_size'], shuffle=True, verbose=1)

        log.info(f'Evaluating fold: {fold_nr}')
        test_loss, test_acc, test_recall, test_precision, test_fscore = model.evaluate(
            x_test, y_test, verbose=1)

        print(f'Accuracy: {(100 * test_acc):.2f}, F1Score: {(100 * test_fscore):.2f}')
        print(f'Precision: {(100 * test_precision):.2f}, Recall: {(100 * test_recall):.2f}')

        accuracy_list.append(test_acc)
        recall_list.append(test_recall)
        precision_list.append(test_precision)
        fscore_list.append(test_fscore)

    print('---> CROSS VALIDATION results:')
    print(f'Accuracy: {np.mean(accuracy_list)} (+/- {np.std(accuracy_list)})')
    print(f'Recall: {np.mean(recall_list)} (+/- {np.std(recall_list)})')
    print(f'Precision: {np.mean(precision_list)} (+/- {np.std(precision_list)})')
    print(f'F1Score: {np.mean(fscore_list)} (+/- {np.std(fscore_list)})')
