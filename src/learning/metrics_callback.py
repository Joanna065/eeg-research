import tensorflow as tf
from tensorflow.keras.metrics import Precision, Recall


class F1Score(tf.keras.metrics.Metric):
    def __init__(self, class_id=0, name='f1score', **kwargs):
        super(F1Score, self).__init__(name=name, **kwargs)
        self.recall = Recall(class_id=class_id, name=f'recall_{class_id}')
        self.precision = Precision(class_id=class_id, name=f'precision_{class_id}')

    def update_state(self, y_true, y_pred, sample_weight=None):
        self.recall.update_state(y_true, y_pred)
        self.precision.update_state(y_true, y_pred)

    def result(self):
        precision = self.precision.result()
        recall = self.recall.result()
        return tf.multiply(2.0, tf.multiply(precision, recall) / tf.add(precision, recall))

    def reset_states(self):
        self.recall.reset_states()
        self.precision.reset_states()


class CategoricalPrecision(tf.keras.metrics.Metric):
    def __init__(self, num_classes, class_id, name='precision', **kwargs):
        super(CategoricalPrecision, self).__init__(name=name, **kwargs)
        self.class_id = class_id
        self.num_classes = num_classes
        self.total_cm = self.add_weight(name="total_confusion_matrix",
                                        shape=(num_classes, num_classes),
                                        initializer="zeros")

    def update_state(self, y_true, y_pred, sample_weight=None):
        self.total_cm.assign_add(confusion_matrix(y_true, y_pred, self.num_classes))

    def result(self):
        cm = self.total_cm
        diag_part = tf.linalg.diag_part(cm)
        precision = diag_part / (tf.reduce_sum(cm, 0) + tf.constant(1e-15))
        return precision[self.class_id]

    def reset_states(self):
        for s in self.variables:
            s.assign(tf.zeros(shape=s.shape))


class CategoricalRecall(tf.keras.metrics.Metric):
    def __init__(self, num_classes, class_id, name='recall', **kwargs):
        super(CategoricalRecall, self).__init__(name=name, **kwargs)
        self.class_id = class_id
        self.num_classes = num_classes
        self.total_cm = self.add_weight(name="total_confusion_matrix",
                                        shape=(num_classes, num_classes),
                                        initializer="zeros")

    def update_state(self, y_true, y_pred, sample_weight=None):
        self.total_cm.assign_add(confusion_matrix(y_true, y_pred, self.num_classes))

    def result(self):
        cm = self.total_cm
        diag_part = tf.linalg.diag_part(cm)
        recall = diag_part / (tf.reduce_sum(cm, 1) + tf.constant(1e-15))
        return recall[self.class_id]

    def reset_states(self):
        for s in self.variables:
            s.assign(tf.zeros(shape=s.shape))


class CategoricalF1Score(tf.keras.metrics.Metric):
    def __init__(self, num_classes, class_id, name='f1score', **kwargs):
        super(CategoricalF1Score, self).__init__(name=name, **kwargs)
        self.recall = CategoricalRecall(class_id=class_id, num_classes=num_classes,
                                        name=f'recall_{class_id}')
        self.precision = CategoricalPrecision(class_id=class_id, num_classes=num_classes,
                                              name=f'precision_{class_id}')

    def update_state(self, y_true, y_pred, sample_weight=None):
        self.recall.update_state(y_true, y_pred)
        self.precision.update_state(y_true, y_pred)

    def result(self):
        precision = self.precision.result()
        recall = self.recall.result()
        f1score = 2 * precision * recall / (precision + recall + tf.constant(1e-15))
        return f1score

    def reset_states(self):
        self.recall.reset_states()
        self.precision.reset_states()


class CategoricalSpecificity(tf.keras.metrics.Metric):
    def __init__(self, num_classes, class_id, name='specificity', **kwargs):
        super(CategoricalSpecificity, self).__init__(name=name, **kwargs)
        self.class_id = class_id
        self.num_classes = num_classes
        self.total_cm = self.add_weight(name="total_confusion_matrix",
                                        shape=(num_classes, num_classes),
                                        initializer="zeros")

    def update_state(self, y_true, y_pred, sample_weight=None):
        self.total_cm.assign_add(confusion_matrix(y_true, y_pred, self.num_classes))
        return self.total_cm

    def result(self):
        cm = self.total_cm
        tp = tf.linalg.diag_part(cm)
        fp = tf.reduce_sum(cm, 0) - tp
        fn = tf.reduce_sum(cm, 1) - tp
        tn = tf.reduce_sum(cm) - (fp + fn + tp)
        specificity = tn / (tn + fp + tf.constant(1e-15))
        return specificity[self.class_id]

    def reset_states(self):
        for s in self.variables:
            s.assign(tf.zeros(shape=s.shape))


def confusion_matrix(y_true, y_pred, num_classes):
    y_true = tf.argmax(y_true, axis=1)
    y_pred = tf.argmax(y_pred, axis=1)
    cm = tf.math.confusion_matrix(y_true, y_pred, dtype=tf.float32, num_classes=num_classes)
    return cm
