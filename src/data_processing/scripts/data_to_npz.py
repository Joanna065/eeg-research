from pathlib import Path

from src.settings import DATA_DIR
from src.utils.io import open_pickle, save_dict_to_npz

DATASET_NAME = 'seizure_2class_new'
TUHEEG_DATASET_PATH = Path(DATA_DIR, 'processed', DATASET_NAME + '.pkl')
SAVE_PATH = Path(DATA_DIR, 'datasets', DATASET_NAME + '.npz')

data = open_pickle(TUHEEG_DATASET_PATH)
labels = data['labels']
samples = data['data']
amount, timeframes, channels, features = samples.shape

print(samples.shape)
samples_reshaped = samples.reshape((amount, timeframes, channels * features))
print(samples_reshaped.shape)

data_dict = {
    'samples': samples_reshaped,
    'labels': labels
}
save_dict_to_npz(SAVE_PATH, data_dict)
