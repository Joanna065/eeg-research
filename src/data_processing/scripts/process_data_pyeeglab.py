import warnings

from pyeeglab import (AbsoluteArea, Bandpower, CommonChannelSet, DynamicWindow, ForkedPreprocessor,
                      Kurtosis, LowestFrequency, Mean, MinMaxCentralizedNormalization, PeakToPeak,
                      Pipeline, Skewness, SpearmanCorrelation, ToDataframe, ToMergedDataframes,
                      ToNumpy, TUHEEGArtifactDataset, Variance, ZeroCrossing)

from src.settings import DATA_DIR
from src.utils.io import save_pickle

# Ignore MNE and TensorFlow warnings
warnings.simplefilter(action='ignore')


def build_data(dataset):
    preprocessing = Pipeline([
        CommonChannelSet(),
        LowestFrequency(),
        ToDataframe(),
        MinMaxCentralizedNormalization(),
        DynamicWindow(8),
        ForkedPreprocessor(
            inputs=[
                SpearmanCorrelation(),
                Mean(),
                Variance(),
                Skewness(),
                Kurtosis(),
                ZeroCrossing(),
                AbsoluteArea(),
                PeakToPeak(),
                Bandpower(['Delta', 'Theta', 'Alpha', 'Beta'])
            ],
            output=ToMergedDataframes()
        ),
        ToNumpy()
    ])

    return dataset.set_pipeline(preprocessing).load()


if __name__ == '__main__':
    DATASET = 'tuheeg_artifact'
    DATASET_DIR = DATA_DIR / 'raw' / DATASET
    # DATASET_DIR = Path('/home/joanna/EEG_data/tuheeg_seizure/')

    dataset = TUHEEGArtifactDataset(str(DATASET_DIR))
    dataset.set_minimum_event_duration(4)
    dataset.index()

    processed_dataset = build_data(dataset)

    common_channels = dataset.maximal_channels_subset
    processed_dataset['common_channels'] = common_channels

    print('--> Common channels:')
    print(common_channels)

    print('--> Data shape:')
    print(processed_dataset['data'].shape)

    SAVE_FILEPATH = DATA_DIR / 'processed' / 'latest' / f'{DATASET}.pkl'
    save_pickle(SAVE_FILEPATH, processed_dataset)
