from pathlib import Path

from src.data_processing.data_utils import check_class_distribution
from src.settings import DATA_DIR
from src.utils.io import load_from_npz

DATASET_NAME = 'tuheeg_abnormal_spearman'
DATASET_PATH = Path(DATA_DIR, 'datasets', DATASET_NAME + '.npz')

data = load_from_npz(DATASET_PATH)
samples, labels = data['samples'], data['labels']

print(check_class_distribution(labels))
