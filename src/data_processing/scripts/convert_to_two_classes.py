from pathlib import Path

import numpy as np

from src.data_processing.process.data_lstm_prepare import open_dataset, save_dataset
from src.settings import DATA_DIR

# ARTIFACT DATASET
DATASET_NAME = 'tuheeg_artifact'
DATASET_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_spearman.pkl')

data, labels, labels_encoder = open_dataset(DATASET_PATH)
print(f'Dataset {DATASET_NAME} shape: {data.shape}')
print(labels_encoder)

labels_changed = np.array([1 if label in [1, 2, 3, 4, 5] else 0 for label in labels])
labels_encoder_changed = ['null', 'artifact']
print(f'Changed encoder: {labels_encoder_changed}')

assert list(np.unique(labels_changed)) == [0, 1], f'More than two classes in {DATASET_NAME}!'
assert len(labels_changed) == len(labels), 'Labels amount after change does not match previous one.'

SAVE_DATA_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_two_classes.pkl')
save_dataset(data, labels_changed, labels_encoder_changed, SAVE_DATA_PATH)

# SEIZURE DATASET
DATASET_NAME = 'tuheeg_seizure'
DATASET_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_spearman.pkl')

data, labels, labels_encoder = open_dataset(DATASET_PATH)
print(f'Dataset {DATASET_NAME} shape: {data.shape}')
print(labels_encoder)

chosen_indices = np.argwhere(np.logical_or(labels == 0, labels == 3))
data_changed = data[chosen_indices].reshape(-1, 8, 19, 30)
print(f'Changed size: {data_changed.shape}')

labels_changed = labels[chosen_indices].flatten()
labels_changed = np.array([1 if label == 3 else 0 for label in labels_changed])
assert list(np.unique(labels_changed)) == [0, 1], f'More than two classes in {DATASET_NAME}!'

labels_encoder_changed = ['fnsz', 'gnsz']
print(f'Changed encoder: {labels_encoder_changed}')

SAVE_DATA_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_two_classes.pkl')
save_dataset(data_changed, labels_changed, labels_encoder_changed, SAVE_DATA_PATH)

# EEG MMIDB DATASET
DATASET_NAME = 'eegmmidb'
DATASET_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_spearman.pkl')

data, labels, labels_encoder = open_dataset(DATASET_PATH)
print(f'Dataset {DATASET_NAME} shape: {data.shape}')
print(labels_encoder)

labels_changed = np.array([1 if label in [1, 2] else 0 for label in labels])
assert list(np.unique(labels_changed)) == [0, 1], f'More than two classes in {DATASET_NAME}!'
assert len(labels_changed) == len(labels), 'CHanged labels amount does not match previous one.'

labels_encoder_changed = ['rest', 'motion']
print(f'Changed encoder: {labels_encoder_changed}')

SAVE_DATA_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_two_classes.pkl')
save_dataset(data, labels_changed, labels_encoder_changed, SAVE_DATA_PATH)

# ABNORMAL DATASET
DATASET_NAME = 'tuheeg_abnormal'
DATASET_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_spearman.pkl')

data, labels, labels_encoder = open_dataset(DATASET_PATH)
print(f'Dataset {DATASET_NAME} shape: {data.shape}')
print(labels_encoder)
assert list(np.unique(labels)) == [0, 1], f'More than two classes in {DATASET_NAME}!'

SAVE_DATA_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_two_classes.pkl')
save_dataset(data, labels, labels_encoder, SAVE_DATA_PATH)

# CHBMIT DATASET
DATASET_NAME = 'chbmit'
DATASET_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_spearman.pkl')

data, labels, labels_encoder = open_dataset(DATASET_PATH)
print(f'Dataset {DATASET_NAME} shape: {data.shape}')
print(labels_encoder)
assert list(np.unique(labels)) == [0, 1], f'More than two classes in {DATASET_NAME}!'

SAVE_DATA_PATH = Path(DATA_DIR, 'processed', f'{DATASET_NAME}_two_classes.pkl')
save_dataset(data, labels, labels_encoder, SAVE_DATA_PATH)
