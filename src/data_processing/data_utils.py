import numpy as np


def check_class_distribution(y_labels):
    unique, counts = np.unique(y_labels, return_counts=True)
    return dict(zip(unique, counts))
