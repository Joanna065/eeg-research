import logging
from pathlib import Path
from typing import List, Tuple

import numpy as np
from sklearn.model_selection import train_test_split

from src.utils.io import open_pickle, save_pickle

log = logging.getLogger(__name__)


def save_dataset(data: np.ndarray, labels: np.ndarray, labels_encoder: List[str],
                 filepath: Path) -> None:
    dataset = {'data': data, 'labels': np.array(labels), 'labels_encoder': labels_encoder}
    save_pickle(filepath, dataset)


def open_dataset(filepath: Path) -> Tuple[np.ndarray, np.ndarray, List[str]]:
    dataset = open_pickle(filepath)
    labels = dataset['labels']
    labels_encoder = dataset['labels_encoder']
    data = dataset['data']

    return data, labels, labels_encoder


def split_data(data, test_size=0.1, shuffle=True):
    samples, labels = data
    log.info("Splitting data")
    x_train, x_test, y_train, y_test = train_test_split(samples, labels, test_size=test_size,
                                                        shuffle=shuffle)
    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=test_size,
                                                      shuffle=shuffle)
    log.debug(f'Split data samples - train: {len(y_train)}, val: {len(y_val)}, test: {len(y_test)}')

    return x_train, y_train, x_val, y_val, x_test, y_test
